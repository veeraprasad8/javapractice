package test;

public class Overriding {


    static {


        System.out.println("parent static block");
    }
    String name;

    {
        name = "parentinstanceblock";
        System.out.println(name);

    }

    Overriding(){
        System.out.println("this is parent class constructor");

    }

    Overriding(int a){
        System.out.println("this is parent class constructor "+a);

    }

    int a = 12;
    double b = 12.232;

    void m1(){

        System.out.println("this is parent classs m1 method");

    }


}
