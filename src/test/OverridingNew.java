package test;

public class OverridingNew extends Overriding {
    int a = 123;
    double c = 121.232;

    static {
        System.out.println("child static block");
    }

    String name;
    {
        name = "instanceblock";
        System.out.println(name);

    }

    OverridingNew (){

       

        System.out.println("this is child class construcoor");

    }

    void m1(){

        System.out.println("this is child classs m1 method");

        System.out.println(a);
        System.out.println(c);
        System.out.println(super.a);// mandatory
        System.out.println(b); // optional
        super.m1();



    }


    public static void main(String[] args) {

        OverridingNew overridingNew = new OverridingNew();
        overridingNew.m1();

    }
}
