package test.Javaprogramlevel1;

public class ReverseProgram {

    public static void main(String[] args) {
        // call reverse string method

        ReverseProgram reverseProgram = new ReverseProgram();
        String StringReverseoutput = reverseProgram.reverseString("123456");
        System.out.println("String reverse : " + StringReverseoutput);
        String dupliteString = reverseProgram.duplicateCharCheckInString("abaccbdfefgg");
        System.out.println("String duplicate : "+dupliteString);


    }

    String reverseString(String input) {
        String reverseValue = input;
        String afterReverse = "";
        for (int i = reverseValue.length() - 1; i >= 0; i--) {
            afterReverse = afterReverse + reverseValue.charAt(i);

        }
        return afterReverse;

    }

    String duplicateCharCheckInString(String str) {

        char strArray[] = str.toCharArray();
        String afterDuplicate = "";
        char charCheck = ' ';
        for (int i = 0; i < strArray.length; i++) {

            for (int j = i + 1; j <strArray.length; j++) {
                if (strArray[i] == strArray[j]) {
                    charCheck = strArray[i];
                    afterDuplicate = afterDuplicate + charCheck;
                    break;

                }
            }


        }
        return afterDuplicate;

    }
}
